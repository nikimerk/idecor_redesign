@extends('layouts.main')

@section('content')

    @include('blocks.carousel')

    @include('blocks.about')

    @include('blocks.skill')

    @include('blocks.instagram')

@endsection
