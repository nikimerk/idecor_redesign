<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 04.06.2019
 * Time: 22:29
 */

namespace App\Admin\Custom\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Images;
use Encore\Admin\Controllers\HasResourceActions;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
	use HasResourceActions;

	/**
	 * @return \Illuminate\Http\JsonResponse|int
	 * AJAX delete image
	 */
	public function deleteImage()
	{
		$id = request()->post('id');
		$image = Images::all(['section', 'name', 'id'])->where('id', $id)->first();

		if ($image) {
			$deleteFile = File::delete(config('app.upload_folders.' . $image->section, 'upload/') . $image->name);
			$deleteColumn = Images::findOrFail($id)->delete();
			if ($deleteFile && $deleteColumn) {
				return response()->json(['status' => 'OK'], 200);
			}
		}

		return http_response_code(403);
	}

}