$( document ).ready (function() {
	$('.close-question').on('click', function () {
		$('#question-block').fadeOut();
	});
	$('.open-question').on('click', function () {
		$('#question-block').fadeIn();
	});
	$('.close-tellform').on('click', function () {
		$('#tell-block').fadeOut();
	});
	$('.open-tellform').on('click', function () {
		$('#tell-block').fadeIn();
	});

	$(function($){
		$('.btn-form-question').click(function(){
			$.post( "send-question",{
				name : $('input[name=name]').val(),
				question: $('input[name=question]').val(),
				email : $('input[name=email]').val()})
				.done(function(data) {
					if(data== 'true'){
						$('input[name=name]').val('');
						$('input[name=question]').val('');
						$('input[name=email]').val('');
						$("[name='qestion-text']").html('Спасибо, Ваше сообщение отправлено');
						setTimeout("$('#question-block').hide()", 2000);
					}
				});
		});
	});

	$(function($){
		$('.btn-form-tell').click(function(){
			$.post( "send-tell",{
				name : $('input[name=name]').val(),
				phone: $('input[name=phone]').val()})
				.done(function(data) {
					if(data== 'true'){
						$('input[name=name]').val('');
						$('input[name=phone]').val('');
						$("[name='qestion-text']").html('Спасибо, Ваше сообщение отправлено');
						setTimeout("$('#tell-block').hide()", 2000);
					}
				});
		});
	});
	$('.btn_tell').on('click', function() {
		$('.wrapper-tell').show();
	});
});