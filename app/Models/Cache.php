<?php

namespace App\Models;

use Illuminate\Support\Facades\Redis;

Class Cache
{
	public static function get($name)
	{
		$result = null;

		if (env('ENABLE_REDIS')) {
			$result = unserialize(Redis::get($name));
		}

		return $result;
	}

	public static function set($name, $value, $expire)
	{
		$result = null;

		if (env('ENABLE_REDIS')) {
			$result = Redis::set($name, serialize($value));
			Redis::expire($name, $expire);
		}

		return $result;
	}
}