<section id="skill" class="skill roomy-100">
    <div class="container">
        <div class="row">
            <div class="skill_bottom_content text-center">
                <div class="col-md-4">
                    <div class="skill_bottom_item">
                        <h2 class="statistic-counter">800</h2>
                        <div class="separator_small"></div>
                        <h5><em>Довольных клиентов</em></h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="skill_bottom_item">
                        <h2 class="statistic-counter">50</h2>
                        <div class="separator_small"></div>
                        <h5><em>Площадок украшено</em></h5>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="skill_bottom_item">
                        <h2 class="statistic-counter">5</h2>
                        <div class="separator_small"></div>
                        <h5><em>Лет опыта</em></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>