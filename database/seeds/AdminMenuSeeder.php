<?php

use Illuminate\Database\Seeder;

class AdminMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_menu')->insert([
            'parent_id' => 0,
            'order' => 0,
            'title' => 'Услуги',
            'icon' => 'fa-briefcase',
            'uri' => '/admin/services',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => 0,
            'order' => 0,
            'title' => 'Локации',
            'icon' => 'fa-institution',
            'uri' => '/admin/locations',
        ]);
        DB::table('admin_menu')->insert([
            'parent_id' => 0,
            'order' => 0,
            'title' => 'Типы локаций',
            'icon' => 'fa-sitemap',
            'uri' => '/admin/location-types',
        ]);
    }
}
