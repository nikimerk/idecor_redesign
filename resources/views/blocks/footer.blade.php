<footer id="footer" class="footer bg-black _position-fixed _bottom-0 ">
    <div class="container">
        <div class="row">
            <div class="main_footer text-center p-top-40 p-bottom-30">
                <p class="wow fadeInRight" data-wow-duration="1s">
                    Студия свадебного декора и свадебных аксессуаров.
                </p>
            </div>
        </div>
    </div>
</footer>