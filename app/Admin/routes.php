<?php

use Illuminate\Routing\Router;

//Admin::registerAuthRoutes();

$attributes = [
    'middleware' => config('admin.route.middleware'),
];


app('router')->group($attributes, function ($router) {

    /* @var \Illuminate\Routing\Router $router */
    $router->namespace('Encore\Admin\Controllers')->group(function ($router) {

        /* @var \Illuminate\Routing\Router $router */
        $router->resource('admin/auth/users', 'UserController')->names('admin.auth.users');
        $router->resource('admin/auth/roles', 'RoleController')->names('admin.auth.roles');
        $router->resource('admin/auth/permissions', 'PermissionController')->names('admin.auth.permissions');
        $router->resource('admin/auth/menu', 'MenuController', ['except' => ['create']])->names('admin.auth.menu');
        $router->resource('auth/menu', 'MenuController', ['except' => ['create']])->names('admin.auth.menu');
        $router->resource('admin/auth/logs', 'LogController', ['only' => ['index', 'destroy']])->names('admin.auth.logs');

    });

    $authController = config('admin.auth.controller', AuthController::class);

    /* @var \Illuminate\Routing\Router $router */
    $router->get('auth/login', $authController.'@getLogin')->name('admin.login');
    $router->post('auth/login', $authController.'@postLogin');
    $router->get('auth/logout', $authController.'@getLogout')->name('admin.logout');
    $router->get('admin/auth/setting', $authController.'@getSetting')->name('admin.setting');
    $router->put('admin/auth/setting', $authController.'@putSetting');
});


Route::group([
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/admin', 'HomeController@index')->name('admin.home');
});

