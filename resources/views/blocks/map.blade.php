<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript">

    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map('map', {
            center: [53.902259076052005, 27.5617262554321],
            zoom: 11,
            controls: ['zoomControl']
        });

        if (address != undefined && address.length) {
            ymaps.geocode('Минск, пр.Победителей 62', {
                results: 1
            }).then(function (res) {
                var firstGeoObject = res.geoObjects.get(0),
                    bounds = firstGeoObject.properties.get('boundedBy');

                firstGeoObject.options.set('preset', 'islands#darkBlueDotIconWithCaption');
                firstGeoObject.properties.set('iconCaption', firstGeoObject.getAddressLine());

                myMap.geoObjects.add(firstGeoObject);
                myMap.setBounds(bounds, {
                    checkZoomRange: true
                });
            });
        }

        if (addressList != undefined && address.length) {

        }
    }

</script>

<div class="main_maps text-center fix">
    <div id="map" class="mapheight"></div>
</div>