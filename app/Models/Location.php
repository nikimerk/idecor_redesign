<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	public $table = 'locations';
	public $primaryKey = 'id';

	public function status()
	{
		return $this->hasOne(LocationTypes::class, 'location_type_id', 'location_type_id');
	}

	protected $fillable = [
		'id',
		'title',
		'sort_num',
		'address',
		'description',
		'title_image',
		'status',
		'location_type_id',
	];

}
