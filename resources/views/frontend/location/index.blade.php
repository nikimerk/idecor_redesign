@extends('layouts.main')

@section('content')

    <section id="location" class="location lightbg">
        <div class="container">
            <div class="row">
                <div class="main_pricing roomy-100">

                    @if ($locations->count() >= 1)
                        @foreach($locations as $location)
                            <div class="col-md-3 col-sm-12">
                                <div class="pricing_item js-click-form">
                                    <form method="POST" action="/locations/{{ @$location->url }}">
                                        <input type="hidden" name="_method" value="GET">
                                        <div class="pricing_price_border"></div>
                                        <div class="pricing_head text-center">
                                            <h4 class="text-uppercase">{{ @$location->title }}</h4>
                                        </div>
                                        <div class="catalog-item-img text-center">
                                            <img src="{{ @$location->image }}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="head_title text-center">
                <h4>Наши работы</h4>
                <div class="separator_auto"></div>
            </div>
        </div>
    </section>

    @include('blocks.map');

@endsection
