<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(LocationTypesTableSeeder::class);
         $this->call(LocationTableSeeder::class);
         $this->call(AdminMenuSeeder::class);
         $this->call(AdminUserSeeder::class);
    }
}
