<nav class="navbar navbar-default navbar-fixed white no-background bootsnav">
    <!-- Start Top Search -->
    <div class="top-search">
        <div class="container">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                <input type="text" class="form-control" placeholder="Search">
                <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
            </div>
        </div>
    </div>
    <!-- End Top Search -->

    <div class="container">
        <!-- Start Atribute Navigation -->
        <div class="attr-nav">
            <ul>
                <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
                <li class="side-menu"><a href="#"><i class="fa fa-bars"></i></a></li>
            </ul>
        </div>
        <!-- End Atribute Navigation -->

        <!-- Start Header Navigation -->
        <div class="navbar-header">
            <a class="navbar-brand" href="#brand">
                <img src="{{ asset('images/logo.png') }}" class="logo logo-pc logo-display" alt="">
                <img src="{{ asset('images/logo.png') }}" class="logo logo-mobile logo-scrolled" alt="">
            </a>
        </div>
        <!-- End Header Navigation -->

        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-right test" data-in="fadeInDown" data-out="fadeOutUp">
                @foreach (config('app.menu') as $section => $item)
                    <li @if ($section == session('activeMenu')) class="active" @endif><a href="{{ @$section }}">{{ @$item['title'] }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>

    <!-- Start Side Menu -->
    <div class="side">
        <a href="#" class="close-side"><i class="fa fa-times"></i></a>
        <div class="widget">

            <a href="/"><h6 class="title">Главная</h6></a>
            <a href="/locations"><h6 class="title">Локации</h6></a>
            <ul class="link">
                @foreach(session('menu')->locations as $url => $title)
                    <li><a href="/locations/{{ @$url }}">{{ @$title }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="widget">
            <h6 class="title">Услуги</h6>
            <ul class="link">
                @foreach(session('menu')->services as $url => $title)
                    <li><a href="/services/{{ @$url }}">{{ @$title }}</a></li>
                @endforeach

            </ul>
        </div>
    </div>
    <!-- End Side Menu -->
</nav>