<script>
	$(document).ready(function(){
		$('.insta_carousel').owlCarousel({
			items: 3,
			margin: 15,
			loop : true,
			dots: true,
            responsive : {
				760 :  {
					items:5,
                }
            }
		})
	});
	hljs.initHighlightingOnLoad();
</script>

<section id="insta_carousel">
    @foreach (\App\Models\HelpersFunction::getInstagramMedias() as $account)
        <div class="container">
            <div class="row carousel-block">
                <div class="owl-carousel owl-theme m-top-30 insta_carousel">
                    @foreach($account as $image)
                        <a href="{{ @$image['link'] }}" target="_blank">
                            <div class="item"><img src="{{ @$image['squareImages'][2] }}"></div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</section>