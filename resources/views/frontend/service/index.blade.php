@extends('layouts.main')

@section('content')

    <section id="location" class="location lightbg">
        <div class="container">
            <div class="row">
                <div class="main_pricing roomy-100">
                    @if ($services->count() >= 1)
                        @foreach($services as $service)
                            <div class="col-md-3 col-sm-12">
                                <div class="pricing_item js-click-form">
                                    <form method="POST" action="/services/{{ @$service->url }}">
                                        <input type="hidden" name="_method" value="GET">
                                        <div class="pricing_price_border"></div>
                                        <div class="pricing_head text-center">
                                            <h4 class="text-uppercase">{{ @$service->title }}</h4>
                                        </div>
                                        <div class="catalog-item-img text-center">
                                            <img src="{{ @$service->image }}">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection
