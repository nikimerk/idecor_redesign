<head>
    <meta charset="utf-8">
    <title>iDecor - Студия свадебного декора и свадебных аксессуаров</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon.ico">

    <!-- JS includes -->
    <script src="{{ asset('redesign/js/vendor/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('redesign/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('redesign/js/jquery.magnific-popup.js') }}"></script>
    <script src="{{ asset('redesign/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('redesign/js/slick.min.js') }}"></script>
    <script src="{{ asset('redesign/js/jquery.collapse.js') }}"></script>
    <script src="{{ asset('redesign/js/bootsnav.js') }}"></script>

    <!-- paradise slider js -->

    <script src="{{ asset('redesign/js/plugins.js') }}"></script>
    <script src="{{ asset('redesign/js/main.js') }}"></script>

    <!--Google Font link-->
    <link href="https://fonts.googleapis.com/css?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap&subset=cyrillic,latin-ext,vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('redesign/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/fonticons.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('redesign/css/bootsnav.css') }}">


    <!--For Plugins external css-->
    <!--<link rel="stylesheet" href="assets/css/plugins.css" />-->

    <!--Theme custom css -->
    <link rel="stylesheet" href="{{ asset('redesign/css/style.css') }}">
    <!--<link rel="stylesheet" href="assets/css/colors/maron.css">-->

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="{{ asset('redesign/css/responsive.css') }}" />

    <script src="{{ asset('redesign/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
</head>