<div class="file-input">
    <div class="file-preview">
        <h5>{{ @$title }}</h5>
        <div class="file-drop-disabled">
            <div class="file-preview-thumbnails">
                @if(!empty($images))
                    @foreach ($images as $image)
                    <div class="js-image-block file-preview-frame krajee-default  kv-preview-thumb" data-fileindex="0" data-template="image">
                        <div class="kv-file-content">
                            <img src="{{ @$image['src'] }}" class="upload-image file-preview-image kv-preview-data">
                        </div>
                        <div class="file-thumbnail-footer">
                            <div class="file-footer-caption">
                                <div class="file-caption-info">{{ @$image['name'] }}</div>
                                <div class="file-size-info"><samp>({{ @$image['size'] }} KB)</samp></div>
                            </div>
                            @if ($deleteBtn)
                                <div class="file-actions">
                                    <div class="file-footer-buttons">
                                        <button type="button" data-url="/admin/location/deleteImage" data-section="{{ @$image['section'] }}" data-id="{{ @$image['id'] }}" class="kv-file-zoom btn btn-sm btn-kv btn-default btn-outline-secondary js-delete-image" title="View Details">Удалить</button>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                @else
                    <div>Отсутствуют загруженные изображения :(</div>
                @endif
            </div>
            <div class="clearfix"></div>
            <div class="file-preview-status text-center text-success"></div>
        </div>
    </div>
</div>
