<?php

namespace App\Http\Middleware;

use Encore\Admin\Middleware\Session;
use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;
use App\Models\Service;
use App\Models\LocationTypes;
use Closure;
use InstagramScraper\Instagram;

class Menu extends Middleware
{
	/**
	 * The names of the cookies that should not be encrypted.
	 *
	 * @var array
	 */
	protected $except = [
		//
	];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		if (empty(session('menu'))) {
			$menu = new \stdClass();
			$menu->services = Service::all()->pluck('title', 'url');
			$menu->locations= LocationTypes::all()->pluck('title', 'url');

			session(['menu' => $menu]);
		}

		$section = $request->route()->getPrefix() ? $request->route()->getPrefix() : '/';
		session(['activeMenu' => $section]);

		return $next($request);
	}
}
