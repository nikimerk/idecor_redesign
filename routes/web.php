<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Models\LocationTypes;

Route::group([
	'middleware' => '\App\Http\Middleware\Menu',
], function (\Illuminate\Routing\Router $router) {
	$router->get('/', 'SiteController@index');
});


Route::group([
	'prefix' =>'locations',
	'middleware' => '\App\Http\Middleware\Menu',
], function (\Illuminate\Routing\Router $router) {
	$router->get('/', 'LocationController@index');
	$router->get('{type}', 'LocationController@list')->where('type', '[A-Za-z]+');
	$router->get('{type}/{id}', 'LocationController@show')->where('type', '[A-Za-z]+')->where('id', '[0-9]+');
});

Route::group([
    'prefix' =>'services',
	'middleware' => '\App\Http\Middleware\Menu',
], function (\Illuminate\Routing\Router $router) {
    $router->get('/', 'ServiceController@index');
    $router->get('{type}', 'ServiceController@show')->where('type', '[A-Za-z]+');
});

Route::group([
    'namespace' => '\App\Admin\Custom\Controllers',
	'middleware' => config('admin.route.middleware'),
], function (\Illuminate\Routing\Router $router) {

	$router->delete('admin/location/deleteImage', 'AdminController@deleteImage');
	$router->resource('admin/locations', 'AdminLocationController');
	$router->resource('admin/services', 'AdminServiceController');
	$router->resource('admin/location-types', 'AdminLocationTypesController');

});
