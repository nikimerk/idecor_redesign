<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Admin::title() }} @if($header) | {{ $header }}@endif</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    {!! Admin::css() !!}
    <link rel="stylesheet" href="{{ url('bootstrap/bootstrap-select/dist/css/bootstrap-select.css') }}"/>
    <link rel="stylesheet" href="{{ url('bootstrap/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" href="{{ url('bootstrap/bootstrap-multiselect/css/multiselect.min.css') }}"/>
    <link rel="stylesheet" href="{{ url('bootstrap/datetimepiker/css/datetimepiker.css') }}">
    <script src="{{ Admin::jQuery() }}"></script>
    {!! Admin::headerJs() !!}
    <script src="{{ url('bootstrap/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script src="{{ url ('redesign/js/admin.js') }}"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="hold-transition {{config('admin.skin')}} {{join(' ', config('admin.layout'))}}">
<div class="wrapper">

    @include('admin::partials.header')

    @include('admin::partials.sidebar')

    <div class="content-wrapper" id="pjax-container">
        <div id="app">
            @yield('content')
        </div>
        {!! Admin::script() !!}
    </div>
    <!-- modal container (required if you need to render dynamic bootstrap modals) -->

</div>


<!-- progress bar js (not required, but cool) -->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/nprogress/0.2.0/nprogress.min.js"></script>--}}

<script>
    function LA() {
    }

    LA.token = "{{ csrf_token() }}";
</script>


<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>

<!-- REQUIRED JS SCRIPTS -->
{!! Admin::js() !!}

</body>
</html>
