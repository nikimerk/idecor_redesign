$(document).ready(function () {
	$('.js-delete-image').click(function () {
		let data = {},
			item = $(this),
			token = $('meta[name="csrf-token"]').attr('content'),
			imageBlock = $(this).closest('.js-image-block');

		data.id = $(item).data('id');

		$.ajax({
			url: $(item).data('url'),
			type: 'DELETE',
			data: data,
			headers: {'X-CSRF-Token': token},
			success: function(result) {
				if (result.status != undefined && result.status == 'OK') {
					$(imageBlock).remove();
				}
			},
			error: function() {
				let groupBlock = $(item).closest('.form-group').find('div').first().find('div').first(),
					error = '<div class="alert alert-danger alert-block ">\n' +
						'<button type="button" class="close" data-dismiss="alert">×</button>\t\n' +
						'<strong class="message">Произошла ошибка при удалении</strong>\n' +
						'</div>';
				$(error).insertAfter(groupBlock);
			}
		});
	});
});