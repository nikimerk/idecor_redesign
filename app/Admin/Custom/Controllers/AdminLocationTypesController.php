<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 04.06.2019
 * Time: 22:29
 */

namespace App\Admin\Custom\Controllers;

use App\Http\Controllers\Controller;
use App\Models\HelpersFunction;
use App\Models\LocationTypes;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Show;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminLocationTypesController extends Controller
{
	use HasResourceActions;

	const UPLOAD_FOLDER = 'upload/content/locationTypes/';

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Content $content)
	{
		return $content
			->header('Список всех типов локаций')
			->body($this->grid());
	}

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid()
	{
		$grid = new Grid(new LocationTypes);

		$grid->location_type_id('ID')->sortable();
		$grid->title('Заголовок')->sortable();
		$grid->url('url')->sortable();
		$grid->sort_num('Порядковый номер')->sortable();
		$grid->column('Статус')->display(function () {
			return HelpersFunction::getStatusCodes($this->status);
		});

		return $grid;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create(Content $content)
	{
		return $content
			->header('Создание локации')
			->description('description')
			->body($this->form());
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form()
	{
		$form = new Form(new LocationTypes);

		$count = LocationTypes::get()->count();

		$form->text('title', 'Заголовок');
		$form->text('url', 'Url');
		$form->text('sort_num', 'Порядковый номер')->value(++$count);
		$form->select('status', 'Статус')->options(HelpersFunction::getStatusCodes())->default(1);
		$form->image('image', 'Заглавное фото');

		return $form;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store()
	{
		$data = request()->post();
		$file = request()->allFiles();

		if (!empty($file['image'])) {
			$image = $file['image'];
			$fileName = time() . '.' . $image->getClientOriginalExtension();
			if ($image->move(static::UPLOAD_FOLDER, $fileName)) {
				$data['image'] = $fileName;
			}
		}

		try {
			LocationTypes::create($data);
		} catch (\Illuminate\Database\QueryException $e) {
			File::delete(static::UPLOAD_FOLDER . $data['image']);
			throw new \Exception($e->getMessage());
		}

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id, Content $content)
	{
		$location = LocationTypes::find($id)->first();

		return $content
			->header('Изменение типа локации')
			->description($location['title'])
			->body($this->formEdit($location));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$data = request()->all();
		$file = request()->allFiles();

		if (!empty($file['new_image'])) {
			$image = $file['new_image'];
			$fileName = time() . '.' . $image->getClientOriginalExtension();

			if ($image->move(static::UPLOAD_FOLDER, $fileName)) {
				$data['image'] = $fileName;
				$itemImage = LocationTypes::find($id)->pluck('image')->first();
				File::delete(static::UPLOAD_FOLDER . $itemImage);
			}
		}

		LocationTypes::find($id)->update($data);
	}

	/**
	 * @param array $values
	 * @return Form
	 */
	protected function formEdit($values = [])
	{
		$form = new Form(new LocationTypes());

		$form->hidden('item_id')->value($values['id']);
		$form->hidden('_method')->value('PATCH');
		$form->text('title', 'Заголовок')->value($values['title']);
		$form->text('url', 'Url')->value($values['url']);
		$form->text('sort_num', 'Порядковый номер')->value($values['sort_num']);
		$form->select('status', 'Статус')->options(HelpersFunction::getStatusCodes())->value($values['status']);
		$form->image('new_image', 'Заглавное фото');
		$images = HelpersFunction::generateImagesBlock([$values['image']], self::UPLOAD_FOLDER, 'Загруженное фото', true);
		$form->html($images);

		return $form;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id, Content $content)
	{
		return $content
			->header('Просмотр типов локации')
			->description('')
			->body($this->detail($id));
	}

	/**
	 * Make a show builder.
	 *
	 * @param mixed $id
	 * @return Show
	 */
	protected function detail($id)
	{
		$item = LocationTypes::findOrFail($id);

		$show = new Show($item);
		$show->title('Заголовок');
		$show->url('Url');
		$show->sort_num('Порядковый номер');
		$show->status('Статус: ' . HelpersFunction::getStatusCodes($item['status']));

		return $show;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$item = LocationTypes::find($id)->first();
		File::delete(self::UPLOAD_FOLDER . $item['image']);
		LocationTypes::destroy($id);
	}
}