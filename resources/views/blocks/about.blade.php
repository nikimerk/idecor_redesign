<section id="about" class="about roomy-100">
    <div class="container">
        <div class="row">
            <div class="main_about">
                <div class="col-md-6">
                    <div class="about_content">
                        <h2>О нас</h2>
                        <div class="separator_left"></div>

                        <p>Мы имеем большой опыт в сфере украшения свадеб.
                            Работаем как в Минска, так и с выездом до 100км.
                            Индивидуальный подход в каждому клиенту.
                            В нашем портфолио множество уже украшеных площадок.
                            Вы можете заказать у нас звонок и в ближайшее время мы вам перезвоним все подробно расскажем.
                        </p>

                        <div class="about_btns m-top-40">
                            <a href="" class="btn btn-primary">Заказать звонок</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about_accordion wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">
                        <div class="faq_main_content">
                            @foreach(session('menu')->services as $url => $title)
                                <h6 class="m-bottom-10"><a href="/services/{{ @$url }}"><i class="fa fa-angle-right"></i> {{ @$title }} </a></h6>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>