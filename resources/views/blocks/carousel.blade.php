@if (!empty($carousesImages = \App\Models\Images::getCarouselSlider()))

    <link href="{{ asset('redesign/modules/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('redesign/modules/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}" rel="stylesheet">
    <script src="{{ asset('redesign/modules/OwlCarousel2-2.3.4/dist/assets/owl.carousel.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.main_carousel').owlCarousel({
                margin:10,
                loop:true,
                autoHeight:true,
                items:1
            })
        });
        hljs.initHighlightingOnLoad();
    </script>

    <section id="corousel">
        <div class="container">
            <div class="row carousel-block">
                <div class="owl-carousel owl-theme m-top-30 main_carousel">
                    @foreach($carousesImages as $image)
                        <div class="item"><img src="{{ @$image }}"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif

