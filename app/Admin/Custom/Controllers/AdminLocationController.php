<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 04.06.2019
 * Time: 22:29
 */

namespace App\Admin\Custom\Controllers;

use App\Http\Controllers\Controller;
use App\Models\HelpersFunction;
use App\Models\Images;
use App\Models\LocationTypes;
use App\Models\Location;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Show;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminLocationController extends Controller
{
	use HasResourceActions;

	const UPLOAD_FOLDER = 'upload/content/locations/';
	const SECTION_NAME = 'location';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Content $content)
    {
		return $content
			->header('Список всех локаций')
			->body($this->grid());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Content $content)
    {
		return $content
			->header('Создание локации')
			->description('description')
			->body($this->form());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
		$data = request()->post();
		$file = request()->allFiles();

		if (!empty($file['title_image'])) {
			$title_image = $file['title_image'];
			$fileName = time() . '.' . $title_image->getClientOriginalExtension();
			if ($title_image->move(static::UPLOAD_FOLDER, $fileName)) {
				$data['title_image'] = $fileName;
			}
		}

		$item = Location::create($data);
		$images = $this->uploadFiles($file['images'], $item['id'], self::SECTION_NAME);
		Images::insert($images);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Content $content)
    {
		return $content
			->header('Просмотр локации')
			->description('description')
			->body($this->detail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Content $content)
    {
		$location = Location::find($id)->first();

		return $content
			->header('Изменение локации')
			->description('description')
			->body($this->formEdit($location));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$data = request()->all();
		$file = request()->allFiles();

		if (!empty($file['new_title_image'])) {
			$itemImages = Location::find($id)->pluck('title_image')->first();

			File::delete(static::UPLOAD_FOLDER . $itemImages);
			$image = $file['new_title_image'];
			$fileName = time() . '.' . $image->getClientOriginalExtension();
			if ($image->move(static::UPLOAD_FOLDER, $fileName)) {
				$data['title_image'] = $fileName;
			}
		}

		Location::find($id)->update($data);

		if (isset($file['new_images'])) {
			$images = $this->uploadFiles($file['new_images'], $id, 'location', self::SECTION_NAME);
			Images::insert($images);
		}

		return redirect('/admin/location/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id)->first();
		File::delete($location['title_image']);
		Images::deleteImages($id, self::SECTION_NAME, self::UPLOAD_FOLDER);
       	Location::destroy($id);
    }

	/**
	 * Make a grid builder.
	 *
	 * @return Grid
	 */
	protected function grid()
	{
		$grid = new Grid(new Location);

		$grid->id('ID')->sortable();
		$grid->title('Заголовок')->sortable();
		$grid->address('Адрес')->sortable();
		$grid->sort_num('Порядковый номер')->sortable();
		$grid->location_type_id('Тип обьекта');
//		$grid->column('Статус')->display(function () {
//			return HelpersFunction::getStatusCodes($this->status);
//		});
		$grid->created_at('Создано')->sortable();
		$grid->updated_at('Обновлено')->sortable();

		return $grid;
	}

	/**
	 * Make a form builder.
	 *
	 * @return Form
	 */
	protected function form()
	{
		$form = new Form(new Location);
		$location_types = LocationTypes::all()->pluck('title', 'location_type_id');
		$count = Location::get()->count();

		$form->text('title', 'Заголовок');
		$form->text('address', 'Адрес');
		$form->text('description', 'Описание');
		$form->text('sort_num', 'Порядковый номер')->value(++$count);
		$form->select('status', 'Статус')->options(HelpersFunction::getStatusCodes())->default(1);
		$form->select('location_type_id', 'Тип обьекта')->options($location_types);
		$form->multipleImage('images', 'Фотографии');
		$form->image('title_image', 'Заглавное фото');

		return $form;
	}

	/**
	 * @param array $values
	 * @return Form
	 */
	protected function formEdit($values = [])
	{
		$form = new Form(new Location);
		$location_types = LocationTypes::all()->pluck('title', 'location_type_id');
		$images = Images::where('item_id' , $values['id'])->where('section', self::SECTION_NAME)->pluck('name', 'id');

		$form->hidden('item_id')->value($values['id']);
		$form->hidden('_method')->value('PATCH');
		$form->text('title', 'Заголовок')->value($values['title']);
		$form->text('address', 'Адрес')->value($values['address']);
		$form->text('description', 'Описание')->value($values['description']);
		$form->text('sort_num', 'Порядковый номер')->value($values['sort_num']);
		$form->select('status', 'Статус')->options(HelpersFunction::getStatusCodes())->value($values['status']);
		$form->select('location_type_id', 'Тип обьекты')->options($location_types)->value($values['location_type_id']);
		$form->multipleImage('new_images', 'Фотографии');
		$form->image('new_title_image', 'Заглавное фото');
		$images = HelpersFunction::generateImagesBlock($images, self::UPLOAD_FOLDER, 'Загруженные фотографии', true);
		$titleImage = HelpersFunction::generateImagesBlock([$values['title_image']], self::UPLOAD_FOLDER, 'Загруженное заглавное фото', true);
		$form->html($images);
		$form->html($titleImage);

		return $form;
	}

	/**
	 * Make a show builder.
	 *
	 * @param mixed $id
	 * @return Show
	 */
	protected function detail($id)
	{
		$item = Location::findOrFail($id);

		$show = new Show($item);
		$show->title('Заголовок');
		$show->address('Адрес');
		$show->description('Описание');
		$show->sort_num('Порядковый номер');
		$show->status('Статус(' . HelpersFunction::getStatusCodes($item['status']) . ')');

		return $show;
	}

	protected function uploadFiles($files = null, $id = '', $section = '')
	{
		$allFiles = [];

		if ($files) {
			foreach ($files as $file) {
				$name = time() . '_' . mt_rand(1000, 5000) . '.' . $file->getClientOriginalExtension();
				$allFiles[] = ['item_id' => $id, 'name' => $name, 'section' => $section];

				$file->move(static::UPLOAD_FOLDER, $name);
			}
		}

		return $allFiles;
	}



}