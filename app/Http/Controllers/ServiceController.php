<?php

namespace App\Http\Controllers;

use App\Admin\Custom\Controllers\AdminServiceController;
use App\Models\Images;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Models\LocationTypes;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    const DEFAULT_IMAGE = '/images/default_location.png';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::all()->where('status', 1);

        foreach ($services as $key => $service) {
            $image = AdminServiceController::UPLOAD_FOLDER . $service->image;
            $services[$key]->image = (File::exists($image) && !empty($service->image)) ? $image : self::DEFAULT_IMAGE;
        }

        return view('frontend.service.index')->with(compact('services'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $uri)
    {
        if (!$item = Service::all()->where('url', $uri)->where('status', 1)->first()) {
            return abort(404);
        }

        $images = Images::where('item_id' , $item['id'])->where('section', 'service')->pluck('name');
		$item['images'] = Images::addUrl($images, AdminServiceController::UPLOAD_FOLDER);

        return view('frontend.service.fotorama')->with(compact('item'));
    }
}
