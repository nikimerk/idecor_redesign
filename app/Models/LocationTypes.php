<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationTypes extends Model
{
    //
	protected $fillable = [
		'location_type_id',
		'title',
		'url',
		'image',
		'sort_num',
		'status',
		];

	public $timestamps = false;

	public $table = 'location_types';
	public $primaryKey = 'location_type_id';
}
