@extends('layouts.main')

@section('content')

    <section id="portfolio" class="portfolio lightbg">
        <div class="container">
            <div class="row">
                <div class="main_portfolio roomy-100">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="head_title text-center">
                            <h2>{{ @$location['title'] }}</h2>
                            <div class="separator_auto"></div>
                            <p>{{ @$location['description'] }}</p>
                        </div>
                    </div>

                    @if (!empty($location['images']))
                        <div class="portfolio_content">
                            @foreach(json_decode($location['images']) as $image)
                                <div class="col-md-3 m-top-30 cursor-pointer">
                                    <div class="portfolio_item">
                                        <img src="{{ asset($image) }}"/>
                                        <div class="portfolio_hover text-center popup-img" href="{{ asset($image) }}"></div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="head_title text-center">
                <h4>{{ @$location['address'] }}</h4>
            </div>
        </div>
    </section>

    <script>
        let address = '{{ @$location['address'] }}';
    </script>

    @include('blocks.map')

@endsection