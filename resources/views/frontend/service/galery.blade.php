@extends('layouts.main')

@section('content')
    <style>
        .mosaicflow__column {
            float:left;
        }
        .mosaicflow__item {
            position:relative;
        }
        .mosaicflow__item img {
            display:block;
            width:100%;
            max-width:500px;
            height:auto;
        }
        .mosaicflow__item p {
            position:absolute;
            bottom:0;
            left:0;
            width:100%;
            margin:0;
            padding:5px;
            background:hsla(0,0%,0%,.5);
            color:#fff;
            font-size:14px;
            text-shadow:1px 1px 1px hsla(0,0%,0%,.75);
            opacity:0;
            -webkit-transition: all 0.4s cubic-bezier(0.23,1,0.32,1);
            -moz-transition: all 0.4s cubic-bezier(0.23,1,0.32,1);
            -o-transition: all 0.4s cubic-bezier(0.23,1,0.32,1);
            transition: all 0.4s cubic-bezier(0.23,1,0.32,1);
        }
        .mosaicflow__item:hover p {
            opacity:1;
        }
    </style>
    <script type="text/javascript" src="{{ asset('redesign/modules/mosaicflow/jquery.mosaicflow.min.js') }}"></script>

    <section>
        <div class="container">
            <div class="row">
                <div class="main_portfolio m-top-20">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="head_title text-center">
                            <h2>{{ @$item['title'] }}</h2>
                            <div class="separator_auto"></div>
                            <p>{{ @$location['description'] }}</p>
                        </div>
                    </div>
                </div>

                <div class="mosaicflow" data-item-height-calculation="attribute">
                    @foreach($item['images'] as $image)
                        <div class="mosaicflow__item">
                            <img width="300" height="300" src="{{ @$image }}"  alt="">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection