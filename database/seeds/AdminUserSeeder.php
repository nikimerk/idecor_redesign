<?php

use Illuminate\Database\Seeder;
use Encore\Admin\Auth\Database\Administrator;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create a user.
        Administrator::truncate();
        Administrator::create([
            'username' => 'idecor',
            'password' => bcrypt('pass'),
            'name'     => 'Administrator',
        ]);
    }
}
