<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $table = 'services';
    public $primaryKey = 'id';

    protected $fillable = [
        'id',
        'title',
        'url',
        'image',
        'sort_num',
        'status',
    ];

}
