@extends('layouts.main')

@section('content')

    <link href="{{ asset('redesign/modules/fotorama-4.6.4/fotorama.css') }}" rel="stylesheet">
    <script type="text/javascript" src="{{ asset('redesign/modules/fotorama-4.6.4/fotorama.js') }}"></script>

    <section class="lightbg">
        <div class="container">
            <div class="row">
                <div class="main_portfolio m-top-20">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="head_title text-center">
                            <h2>{{ @$item['title'] }}</h2>
                            <div class="separator_auto"></div>
                        </div>
                    </div>
                </div>

                <div class="fotorama" data-nav="thumbs" data-width="700" data-ratio="700/467" data-max-width="100%">
                    @foreach($item['images'] as $image)
                        <img src="{{ @$image }}">
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection