var $button = $('#menu-btn');

$button.on('click', function(e){
    e.preventDefault();
    if( $button.hasClass('open') ){

        $('#dl-menu').removeClass('_right500');
      $button.removeClass('open');
      $button.addClass('close');
      $('body').css({'overflow' : 'overlay'});
        $('.wrapper').removeClass('transform-wrapper');

    } else {

        $('#dl-menu').addClass('_right500');
      $button.removeClass('close');
      $button.addClass('open');
      $('body').css({'overflow' : 'overlay'});
      $('.wrapper').addClass('transform-wrapper');
    }
});