@extends('layouts.main')

@section('content')

    <section id="blog" class="blog">
        <div class="container">
            <div class="row">
                <div class="main_blog text-center roomy-40">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="head_title text-center">
                            <h2>{{ @$section['title'] }}</h2>
                            <div class="separator_auto"></div>
                        </div>
                    </div>
                    @foreach($locations as $location)
                        <div class="col-md-4 cursor-pointer js-click-form">
                            <form method="POST" action="/locations/{{ $section['url'] . '/' . $location['id'] }}">
                                <input name="_method" type="hidden" value="GET">
                                <div class="blog_item m-top-20">
                                    <div class="blog_item_img">
                                        <img src="{{ @$location['title_image'] }}" />
                                    </div>
                                    <div class="blog_text roomy-20">
                                        <h5>{{ @$location['title'] }}</h5>
                                        <p><em>{{ @$location['address'] }}</em></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection
