<?php

namespace App\Http\Controllers;

use App\Admin\Custom\Controllers\AdminLocationController;
use App\Admin\Custom\Controllers\AdminLocationTypesController;
use App\Models\Images;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Models\LocationTypes;
use Illuminate\Support\Facades\File;

class LocationController extends Controller
{
    const DEFAULT_IMAGE = '/images/default_location.png';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$items = LocationTypes::all()->where('status', 1);
		$locations = $items;
		$locationsList = $items->pluck('title');
        foreach ($locations as $key => $location) {
            $image = AdminLocationTypesController::UPLOAD_FOLDER . $location->image;
            $locations[$key]->image = (File::exists($image) && !empty($location->image)) ? $image : self::DEFAULT_IMAGE;
        }

		return view('frontend.location.index')->with(compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request, $type)
    {

		$section = LocationTypes::all()->where('url', $type)->where('status', 1)->first();
		$locations = Location::all()->where('location_type_id', $section['location_type_id'])->where('status', 1);
		if (!$section || !$locations) {
			return abort(404);
		}

		if (!empty($locations)) {
			foreach ($locations as $key => $location){
                $image = AdminLocationController::UPLOAD_FOLDER . $location->title_image;
				$locations[$key]['title_image'] = (File::exists($image) && !empty($location->title_image)) ? $image : self::DEFAULT_IMAGE;
			}
		}

		return view('frontend.location.store')->with(compact('section', 'locations'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $type,  $id)
    {
		$location = Location::all()->where('id', $id)->where('status', 1)->first();

		if (!LocationTypes::all()->where('url', $type)->where('status', 1)->first() || !$location) {
			return abort(404);
		}

		$images = Images::where('id' , $id)->where('section', 'location')->pluck('name');
		$location['images'] = Images::addUrl($images, AdminLocationController::UPLOAD_FOLDER);

		return view('frontend.location.show')->with(compact('location'));
    }
}
