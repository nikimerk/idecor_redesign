<?php

use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('locations')->insert([
			'title' => 'Golden coffee',
			'address' => 'Минск, ул. Максима Богдановича 26',
			'title_image' => '',
			'sort_num' => '1',
			'status' => '1',
			'location_type_id' => '1',
		]);
		DB::table('locations')->insert([
			'title' => 'Bergamo',
			'address' => 'Минск, ул. Кульман 37',
			'title_image' => '',
			'sort_num' => '1',
			'status' => '1',
			'location_type_id' => '2',
		]);
	}
}
