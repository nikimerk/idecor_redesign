<?php
/**
 * Created by PhpStorm.
 * User: Nikita
 * Date: 04.06.2019
 * Time: 22:29
 */

namespace App\Admin\Custom\Controllers;

use App\Http\Controllers\Controller;
use App\Models\HelpersFunction;
use App\Models\Images;
use App\Models\LocationTypes;
use App\Models\Location;
use App\Models\Service;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Middleware\Session;
use Encore\Admin\Show;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminServiceController extends Controller
{
    use HasResourceActions;

    const UPLOAD_FOLDER = 'upload/content/service/';
	const SECTION_NAME = 'service';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Content $content)
    {
        return $content
            ->header('Список всех услуг')
            ->body($this->grid());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Content $content)
    {
        return $content
            ->header('Создание услуги')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = request()->post();
        $file = request()->allFiles();

        if (!empty($file['title_image'])) {
            $title_image = $file['title_image'];
            $fileName = time() . '.' . $title_image->getClientOriginalExtension();
            if ($title_image->move(static::UPLOAD_FOLDER, $fileName)) {
                $data['image'] = $fileName;
            }
        }

        $item = Service::create($data);
        $images = $this->uploadFiles($file['images'], $item['id'], self::SECTION_NAME);
        Images::insert($images);
		session()->forget('menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Content $content)
    {
        $service = Service::find($id)->first();

        return $content
            ->header('Просмотр услуги')
            ->description($service->title)
            ->body($this->detail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Content $content)
    {
        $service = Service::find($id)->first();

        return $content
            ->header('Изменение услуги')
            ->description($service->title)
            ->body($this->formEdit($service));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->all();
        $file = request()->allFiles();

        if (!empty($file['new_title_image'])) {
            $itemImages = Service::find($id)->pluck('image')->first();

            File::delete(static::UPLOAD_FOLDER . $itemImages);
            $image = $file['new_title_image'];
            $fileName = time() . '.' . $image->getClientOriginalExtension();
            if ($image->move(static::UPLOAD_FOLDER, $fileName)) {
                $data['image'] = $fileName;
            }
        }

        Service::find($id)->update($data);

        if (isset($file['new_images'])) {
            $images = $this->uploadFiles($file['new_images'], $id, self::SECTION_NAME);
            Images::insert($images);
        }

        return redirect('/admin/location/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Service::find($id)->first();
        File::delete(self::UPLOAD_FOLDER . $item['image']);
        Images::deleteImages($id, self::SECTION_NAME, self::UPLOAD_FOLDER);
		Service::destroy($id);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Service());

        $grid->id('ID')->sortable();
        $grid->title('Заголовок')->sortable();
        $grid->url('Url')->sortable();
        $grid->sort_num('Порядковый номер')->sortable();
        $grid->column('Статус')->display(function () {
            return HelpersFunction::getStatusCodes($this->status);
        });
        $grid->created_at('Создано')->sortable();
        $grid->updated_at('Обновлено')->sortable();

        return $grid;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Service());
        $count = Service::get()->count();

        $form->text('title', 'Заголовок');
        $form->text('url', 'Url');
        $form->text('sort_num', 'Порядковый номер')->value(++$count);
        $form->select('status', 'Статус')->options(HelpersFunction::getStatusCodes())->default(1);
        $form->multipleImage('images', 'Фотографии');
        $form->image('title_image', 'Заглавное фото');

        return $form;
    }

    /**
     * @param array $values
     * @return Form
     */
    protected function formEdit($values = [])
    {
        $form = new Form(new Service());
        $images = Images::where('item_id' , $values['id'])->where('section', self::SECTION_NAME)->pluck('name', 'id');

        $form->hidden('item_id')->value($values['id']);
        $form->hidden('_method')->value('PATCH');
        $form->text('title', 'Заголовок')->value($values['title']);
        $form->text('sort_num', 'Порядковый номер')->value($values['sort_num']);
        $form->select('status', 'Статус')->options(HelpersFunction::getStatusCodes())->value($values['status']);

        $form->multipleImage('new_images', 'Фотографии');
        $form->image('new_title_image', 'Заглавное фото');
        $images = HelpersFunction::generateImagesBlock($images, self::UPLOAD_FOLDER, 'Загруженные фотографии', true);
        $titleImage = HelpersFunction::generateImagesBlock([$values['title_image']], self::UPLOAD_FOLDER, 'Загруженное заглавное фото', true);
        $form->html($images);
        $form->html($titleImage);

        return $form;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $item = Service::findOrFail($id);

        $show = new Show($item);
        $show->title('Заголовок');
        $show->url('Url');
        $show->sort_num('Порядковый номер');
        $show->status('Статус : ' . HelpersFunction::getStatusCodes($item['status']));

        return $show;
    }

    protected function uploadFiles($files = null, $id = '', $section = '')
    {
        $allFiles = [];

        if ($files) {
            foreach ($files as $file) {
                $name = time() . '_' . mt_rand(1000, 5000) . '.' . $file->getClientOriginalExtension();
                $allFiles[] = ['item_id' => $id, 'name' => $name, 'section' => $section];

                $file->move(static::UPLOAD_FOLDER, $name);
            }
        }

        return $allFiles;
    }

}