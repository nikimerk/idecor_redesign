<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use InstagramScraper\Instagram;

class HelpersFunction extends Model
{
    public static $statusCodes = [
        0 => 'Скрыт',
        1 => 'Виден'
    ];

    /**
     * @return array
     */
    public static function getStatusCodes($code = null)
    {
        if (is_null($code)) {
            return self::$statusCodes;
        }

        if (isset(self::$statusCodes[$code])) {
            return self::$statusCodes[$code];
        }
    }

	/**
	 * generate images block for admin
	 * @param $names
	 * @param $folder
	 * @param string $title
	 * @param bool $deleteBtn
	 * @return $this
	 */
	public static function generateImagesBlock($names, $folder, $title = '', $deleteBtn = false)
	{
		$images = [];

		foreach ($names as $key => $name){
			if (empty($name)) {
				break;
			}

			$images[$key] = [
				'id' => $key,
				'name' => $name,
				'src' => asset($folder . $name),
				'size' => round(File::size(public_path($folder . $name))/1024, 2),
			];
		}


		return view('admin.partials.imagesBlock')->with(compact('images', 'title', 'deleteBtn'));
	}

	/**
	 * @return array
	 * @throws \InstagramScraper\Exception\InstagramException
	 * @throws \InstagramScraper\Exception\InstagramNotFoundException
	 */
	public static function getInstagramMedias()
	{
		$medias = [];
		$inst = new Instagram();
		Instagram::setProxy([
			'address' => '192.168.15.240',
			'port'    => '3128',
			'tunnel'  => true,
			'timeout' => 30,
		]);

		$accounts = config('app.instagram_accounts');

		if ($accounts) {
			foreach ($accounts as $account => $count) {
				try {
					$medias[$account] = $inst->getMedias($account, $count);
				} catch (\Exception $e) {
				}
			}
		}

		return $medias;
	}

}
