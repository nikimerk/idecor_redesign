<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    {{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'iDecor') }}</title>

    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    {{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
    {{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}

    <!-- Styles -->
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}



    <meta id="meta_vp">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Merkulov Nikita">
    <meta name="contact" content="nikitamerkulovv@gmail.com">

    <script src="{{ asset('js/additional/jquery.min.js') }}"></script>

    <link rel="stylesheet" href="{{  asset('css/main.css') }}" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.css" type="text/css">
    <script src="{{ asset('js/main.js') }}"></script>

    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{ asset('modules/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('modules/owlcarousel/assets/owl.theme.default.min.css') }}">

    <script src="{{ asset('modules/owlcarousel/owl.carousel.js') }}"></script>


    <!--    desctop-menu-->
    <link rel="stylesheet" href="{{ asset('modules/flat-menu/style.css') }}" type="text/css" media="screen">
    <link rel="stylesheet" href="{{ asset('modules/flat-menu/font-awesome/css/font-awesome.css') }}" >
    <!--    desctop-menu end-->

    <!-- Мобильное меню -->
    <meta name="description" content="Responsive Multi-Level Menu: Space-saving drop-down menu with subtle effects" />
    <meta name="keywords" content="multi-level menu, mobile menu, responsive, space-saving, drop-down menu, css, jquery" />
    <!-- <link rel="stylesheet" type="text/css" href="css/default.css" /> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/additional/component_menu.css') }}" />
    <script src="{{ asset('js/additional/modernizr.custom.js') }}"></script>
    <!-- Мобильное меню-end -->


    <!-- mob-menu-gamburger -->
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Candal'>
    <link rel="stylesheet" href="{{ asset('css/additional/style_gamburger.css') }}">
    <!-- mob-menu-gamburger end -->

    <link rel="stylesheet" href="{{ asset('css/additional/docs.theme.min.css') }}">

    <!-- VK Widget -->
    {{--<script type="text/javascript" src="https://vk.com/js/api/openapi.js?159"></script>--}}
    {{--<script type="text/javascript"> VK.Widgets.CommunityMessages("vk_community_messages", 65136682, {expanded: "1",tooltipButtonText: "Есть вопрос?"}); </script>--}}
    <!-- VK Widget -->

</head>
<body>

<div id="vk_community_messages"></div>

<a class="open-question"><img class="open-question"  src="{{ asset('img/logo/question.png') }}"></a>
<div id="question-block">
    <form class="transparent" id="question-form" method="POST" onsubmit="return false;">
        <div class="form-inner">
            <a class="close-question"><img style="" class="close-question"  src="{{ asset('img/logo/cancel_icon.png') }}"></a>
            <h3>Отправь нам свой вопрос</h3>
            <label>Имя</label>
            <input type="text" name="name">
            <label>Вопрос</label>
            <input name="question" type="text">
            <label>Адрес эл.почты</label>
            <input name="email" type="email">
            <input class="btn-form-question" type="submit" value="Отправить">
        </div>
    </form>
</div>

<div id="tell-block" class="wrapper-tell">
    <form class="transparent" id="tellform" method="POST" onsubmit="return false;">
        <div class="form-inner">
            <a class="close-tellform"><img class="close-tellform"  src="{{ asset('img/logo/cancel_icon.png') }}"></a>
            <h3>Укажите свой номер и мы вам перезвоним</h3>
            <label>Имя</label>
            <input type="text" name="name">
            <label>Номер телефона</label>
            <input maxlength="20" name="phone" type="tel">
            <input class="btn-form-tell" type="submit" value="Отправить">
        </div>
    </form>
</div>

<div class="header d-none d-lg-block">
    <div class="logo">
        <img src="{{ asset('img/logo/imperial_logo.png') }}">
    </div>
    <div class="menu">
        <div class="social">
            <a href="https://vk.com/club65136682" target="_blank"><img src="{{ asset('img/logo/vk-ico.png') }}"></a>
            <a href="https://www.instagram.com/imperialminsk/" target="_blank"><img src="{{ asset('img/logo/instagram-ico.png') }}"></a>
            <span id="title">Студия свадебного оформления и свадебных аксессуаров</span>
            <span><label for="button-tell-me" class="btn_tell">Заказать звонок</label></span>
            <div id="time_job">
                <div>C 9:00 до 23:00 ежедневно</div>
                <div id="tell">
                    <?php /* ?>
                <?php foreach ($users as $user) { ?>
                    <a style="display:block;color: #605E5E!important;" href="tel:<?php echo $user['phone'] ?>"><?php echo $user['phone'] ?></a>
                <?php } ?>
                <?php */ ?>
                </div>
            </div>
        </div>
    </div>
    <div class="desctop-menu">
        <nav>
            <ul class="nav desc-menu">
                <li><a class="menu-a" href="/"></i>Главная</a></li>
                <li><a class="menu-a" href="/catalog"></i>Каталог</a></li>
                <li><a class="menu-a" href="/location"></i>Места</a>
                    <ul class="sub-menu">
                        <li><a href="/cottages">Усадьбы и коттеджи</a></li>
                        <li><a href="/restaurants">Рестораны</a></li>
                        <li><a href="/cafe">Кафе</a></li>
                        <li><a href="/public-catering">Общепит. заведения</a></li>
                    </ul>
                </li>
                <li><a class="menu-a" href="/prices"></i>Цены</a></li>
                <li><a class="menu-a" href="/contacts"></i>О нас</a></li>
            </ul>
        </nav>
    </div>
</div>

<div class="wrapper">
    <div class="container">
        <div class="header-mobile d-lg-none">
            <p><a href="#" id="trigger" class="menu-trigger"></a></p>
            <img style="height: 350px; margin-left: 280px" src="../../img/logo/imperial_logo.png">
            <div class="overflow"></div>
            <div id="dl-menu" class="dl-menuwrapper">
                <!-- mob-menu-gamburger -->
                <div id="menu-btn">
                    <span class="icon"></span>
                    <span class="text">Меню</span>
                </div>
                <!-- mob-menu-gamburger end-->

                <ul class="dl-menu">
                    <li><a href="/">Главная</a></li>
                    <li>
                        <a href="/catalog">Каталог</a>
                    </li>
                    <li>
                        <a href="#">Места</a>
                        <ul class="dl-submenu">
                            <li class="dl-back"><a href="#">Назад</a></li>
                            <li><a href="/cottages">Усадьбы</a></li>
                            <li><a href="/restaurants">Рестораны</a></li>
                            <li><a href="/cafe">Кафе</a></li>
                            <li><a href="/public-catering">Общепит</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/prices">Цены</a>
                    </li>
                    <li><a href="/contacts">О нас</a></li>
                </ul>
            </div><!-- /dl-menuwrapper -->



            <h2>Студия свадебных украшений и свадебных аксессуаров</h2>
            <br>
            <h2>
                с 11:00 до 20:00 (ежедневно)</h2>
                <?php /* ?>
                    <?php foreach ($users as $user) { ?>
                        <h2><a style="display:block;min-width: 300px;color: #605E5E!important;" href="tel:<?php echo $user['phone'] ?>"><?php echo $user['phone'] ?></a></h2>
                    <?php } ?>
                <?php */ ?>
            <br>
            <span><button class="btn_tell shadow2">Заказать звонок</button></span>
        </div>


        @yield('content')

    </div>
</div>

<footer>
    <div class="footer shadow">
        <div class="follow-footer">
            <p>Follow</p>
            <a href="https://vk.com/club65136682">
                <img class="social-logo" src="{{ asset('img/logo/vk-incon2.png') }}">
            </a>
            <a href="http://instagram.com/imperialminsk">
                <img class="social-logo" src="{{ asset('img/logo/instagram-logo.png') }}">
            </a>
        </div>
        <div class="title">
            <h4 id="footer-title">Студия свадебного декора и свадебных аксессуаров</h4>
        </div>
        <div class="contact">
            <a class="fa-phone" style="color: white" href="tel:+375 (44) 731-84-63"><h4>+375 (44) 731-84-63</h4></a>
            <a class="fa-mail"><h4>idecorminsk@yandex.ru</h4></a>
        </div>
    </div>
</footer>



<!-- скрип моб меню -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script src="{{ asset('js/additional/jquery.dlmenu.js') }}"></script>
<script>
    $(function() {
        $( '#dl-menu' ).dlmenu({
            animationClasses : { in : 'dl-animate-in-5', out : 'dl-animate-out-5' }
        });
    });
</script>
<!-- скрип моб меню-end -->

<!-- mob-menu-gamburger -->
<!-- <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script> -->
<script  src="{{ asset('js/additional/index_gamburger.js') }}"></script>
<!-- mob-menu-gamburger end -->

</body>
</html>
