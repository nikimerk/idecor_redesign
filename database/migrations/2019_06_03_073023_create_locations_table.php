<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->bigIncrements('id')->unique()->index();
            $table->string('title')->default('');
            $table->string('address')->default('');
            $table->string('sort_num')->default(1);
            $table->string('description')->default('');
            $table->string('title_image')->default('');
            $table->integer('status')->default(1);
            $table->string('location_type_id');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
