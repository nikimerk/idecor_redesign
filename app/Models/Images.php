<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Images extends Model
{

	const CACHE_CAROUSEL_SLIDER = 'carouselSlider';
	const CAROUSEL_STORAGE_DURATION = 60*60; // (60 sec * 60 min = 1 hour)

	protected $fillable = [
		'id',
		'item_id',
		'name',
		'section',
	];

	public static function addUrl($images = [], $path = 'upload/') {

		if (!empty($images)) {
			foreach ($images as $key => $name) {
				if (!File::exists(asset($path . $name))) {
					unset($images[$key]);
					continue;
				}
				$images[$key] = asset($path . $name);
			}
		}

		return $images;

	}

	public static function deleteImages($id, $section, $path) {

		$images = self::all()->where('item_id', $id)->where('section', $section);

		if (!empty($images)) {
			foreach ($images as $image) {
				File::delete($path . $image->name);
			}
		}

		$images = $images->pluck('id', 'id');

		self::destroy($images);

	}

	public static function getCarouselSlider() {

		$slider = Cache::get(self::CACHE_CAROUSEL_SLIDER);

		if (!$slider) {
			$names = Images::all()->pluck('name', 'id');
			$slider = Images::addUrl($names, config('app.upload_folders.carousel'));
			Cache::set(self::CACHE_CAROUSEL_SLIDER, $slider, self::CAROUSEL_STORAGE_DURATION);
		}

		return $slider;
	}

}
