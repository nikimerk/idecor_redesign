<?php

use Illuminate\Database\Seeder;

class LocationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('location_types')->insert([
			'title' => 'Кафе',
			'url' => 'cafe',
			'image' => '',
			'sort_num' => '1',
		]);
		DB::table('location_types')->insert([
			'title' => 'Рестораны',
			'url' => 'restaurants',
			'image' => '',
			'sort_num' => '1',
		]);
    }
}
